# Fulstack Developer

---

This repository is build to record my journey as i learn to become a fulstack node developer. The main stack is compose this following way:

## The Full program

1. vanilla javascript

   - core of javascript
   - modern feautures of javascript
   - Dom manipulation

2. node & express

   - project based
   - http request
   - canva api
   - CLI tools nodejs
   - database
   - authentification
   - testing
   - mvc nodejs structure
